FROM  python:3.7

LABEL description="Stack Tests" owner="security@biosme.com" version="0.0.1"

RUN apt-get update && apt-get upgrade -y && apt-get install openssh-client -y

RUN useradd -ms /bin/bash cnross
COPY requirements.txt ./
RUN pip install -r requirements.txt
USER cnross
WORKDIR /home/cnross
RUN mkdir -p  /home/cnross/.ssh
COPY .ssh/ /home/cnross/.ssh/
COPY ./main.py  ./
COPY ./tests/ ./
ENV SLACK_HOOK="https://hooks.slack.com/services/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"

CMD pytest -sv --slack_channel=secops --slack_hook=$SLACK_HOOK --slack_username="NMDC end to end test"
