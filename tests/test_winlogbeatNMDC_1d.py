from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search


def test_winlogbeatNMDC_1D():
    """Calls ES and check count for winlogbeat"""

    ES_PROD = Elasticsearch(
        ['https://localhost:9200'],
        http_auth=('elastic', 'j7rsmhgo7yhdyVkbQVwB'),
        use_ssl=False,
        verify_certs=False)
    INDEX="winlogbeat-7.4*"
    ENV_ID="nmdc"

    daily_search = Search(using=ES_PROD,
                          index=INDEX)\
        .query("term", env_id=ENV_ID).\
        filter('range', **{'@timestamp': {'gte': 'now-1d', 'lt': 'now'}})\
        .count()
    #print(daily_search)
    assert daily_search > 1, 'Winlogbeat events have been seen in the last 1d days.'


#test_winlogbeatNMDC_1D()