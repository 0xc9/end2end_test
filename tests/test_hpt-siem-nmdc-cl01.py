from fabric import Connection


def test_nmdc_cl02():
    """Calls remote server over ssh and checks Logstash API available"""
    result = Connection('nmdc01').run('curl -XGET localhost:9600/?pretty', hide=True)
    assert result.ok == True, 'Logstash reachable on NMDC collector 1'
    return(result.ok)

