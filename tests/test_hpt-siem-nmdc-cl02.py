from fabric import Connection


def test_nmdc_cl01():
    """Calls remote server over ssh and checks Logstash API available"""
    result = Connection('nmdc02').run('curl -XGET localhost:9600/?pretty', hide=True)
    assert result.ok == True, 'Logstash reachable on NMDC collector 2'
    return(result.ok)

