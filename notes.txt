GET winlogbeat-7.3.2-2019.11*/_count
{
    "query": {
    "bool": {
      "must": [],
      "filter": [
        {
          "match_all": {}
        },
        {
          "range": {
            "@timestamp": {
              "format": "strict_date_optional_time",
              "gte": "2019-11-01T06:25:05.831Z",
              "lte": "2019-12-01T06:25:15.149Z"
            }
          }
        }
      ],
      "should": [],
      "must_not": [
        {
          "match_phrase": {
            "env_id": {
              "query": "fiserv"
            }
          }
        }
      ]
    }
  }
}
